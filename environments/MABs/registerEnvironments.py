import gymnasium
from gymnasium.envs.registration import  register
import numpy as np



"""
This file contains methods to register several MDP environments into gym.
Name of environments should not contain symbol '_'. 

"""
def registerBernBandit(means,  max_steps=np.infty,  reward_threshold=1.):
    name = 'MAB-Bernoulli-v0'
    register(
        id=name,
        entry_point='environments.MABs.StochasticBandits:BernoulliBandit',
        max_episode_steps=max_steps,
        reward_threshold=reward_threshold,
        kwargs={'means': means, 'name':name }
    )
    return name




registerWorlds = {
    "mab-bernoulli": lambda x: registerBernBandit(means= [0.2,0.8,0.3,0.7]),
}


def register_MABs(envName):
    if (envName in registerWorlds.keys()):
        regName = (registerWorlds[envName])(0)
        print("[INFO] Environment " + envName + " registered as " + regName)
        return regName

def makeWorld(registername):
    """

    :param registername: name of the environment to be registered into gym
    :return:  full name of the registered environment
    """
    return gymnasium.make(registername)