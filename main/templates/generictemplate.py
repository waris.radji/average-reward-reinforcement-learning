from experiments.fullExperiment import runLargeMulticoreExperiment

#######################
# Import registered environments
import environments.MDPs_discrete.registerEnvironments as bW
# To get the list of registered environments:
#print("List of registered environments: ")
#[print(k) for k in bW.registerWorlds.keys()]

# or Import directly some environment

#######################
# Import some learners
import learners.Generic.Random as random
import learners.Generic.Qlearning as ql
import learners.MDPs_discrete.UCRL3 as ucrl3
import learners.MDPs_discrete.IMED_RL as imedrl
import learners.MDPs_discrete.Optimal.OptimalControl  as opt



#######################
# Instantiate one environment
#env = bW.makeWorld(bW.registerWorld('grid-2-room'))
#env = bW.makeWorld(bW.registerWorld('grid-4-room'))
env = bW.makeWorld(bW.register_MDPs_discrete('river-swim-6'))
#env = bW.makeWorld(bW.registerWorld('grid-random'))

#######################
# Instantiate a few learners to be compared:
agents = []
nS = env.observation_space.n
nA = env.action_space.n
delta=0.05
agents.append( [random.Random, {"env": env.env}])
agents.append( [ql.Qlearning, {"nS":nS, "nA":nA}])
agents.append( [ucrl3.UCRL3_lazy, {"nS":nS, "nA":nA, "delta":delta}])
agents.append(([imedrl.IMEDRL, {"nbr_states":nS, "nbr_actions":nA}]))

#############################
# Compute oracle policy:
oracle = opt.build_opti(env.name, env.env, env.observation_space.n, env.action_space.n)

#######################
# Run a full experiment
#######################
import os
from experiments.utils import get_project_root_dir
ROOT= get_project_root_dir()+"/main/templates/"+"generictemplate"+"_results/"
os.mkdir(ROOT)

runLargeMulticoreExperiment(env, agents, oracle, timeHorizon=5000, nbReplicates=16,root_folder=ROOT)

#######################
# Plotting Regret directly from dump files of past runs:
#files = plR.search_dump_cumRegretfiles("RiverSwim-6-v0", root_folder=ROOT)
#plR.plot_results_from_dump(files, 500)
